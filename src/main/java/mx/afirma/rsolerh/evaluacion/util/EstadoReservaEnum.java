package mx.afirma.rsolerh.evaluacion.util;

import lombok.Getter;

@Getter
public enum EstadoReservaEnum {
	
	PENDIENTE("pendiente", 1),
	CONFIRMADA("confirmada", 2),
	CANCELADA("cancelada", 3);
	
	private String value;
	private Integer key;
	
	private EstadoReservaEnum(String value, Integer key) {
		this.value = value;
		this.key = key;
	}
	
	public static EstadoReservaEnum getEnumByKey(Integer key) {
		for ( EstadoReservaEnum v : values() ) {
			if(v.key.equals(key)) return v;
		}
		return null;
	}

}
