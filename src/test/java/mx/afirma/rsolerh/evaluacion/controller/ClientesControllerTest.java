package mx.afirma.rsolerh.evaluacion.controller;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.util.LinkedMultiValueMap;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import mx.afirma.rsolerh.evaluacion.model.request.ClienteRequest;
import mx.afirma.rsolerh.evaluacion.service.IClienteService;


@WebMvcTest(ClientesController.class)
@AutoConfigureMockMvc(addFilters = false)
class ClientesControllerTest {
	
	@Autowired private MockMvc mvc;
	
	@MockBean private IClienteService clienteServ;
	
	ObjectMapper objectMapper = new ObjectMapper();


	@Test
	void testGetAllClients() throws Exception {
		
		when(clienteServ.getAllClients())
		.thenReturn(new ArrayList<>());
		
		mvc.perform(MockMvcRequestBuilders
	  			.get("/api/clientes/all")
	  			.accept(MediaType.APPLICATION_JSON)
	  			.contentType(MediaType.APPLICATION_JSON))
//	      .andDo(print())
	      .andExpect(status().isOk());

	}

	
	@Test
	void testAddClient() throws Exception {
		when(clienteServ.addClient(any()))
		.thenReturn(10L);
		ClienteRequest request = new ClienteRequest();
		request.setNombre("Richard");
		request.setApellido("Soler");
		request.setEmail("risolerh@gmail.com");
		
		mvc.perform(MockMvcRequestBuilders
	  			.post("/api/clientes")
	  			.content(objectMapper.writeValueAsBytes(request))
	  			.accept(MediaType.APPLICATION_JSON)
	  			.contentType(MediaType.APPLICATION_JSON))
//	      .andDo(print())
	      .andExpect(status().isOk());

	}

	
	@Test
	void testAddClientBad() throws Exception {
		when(clienteServ.addClient(any()))
		.thenReturn(10L);
		ClienteRequest request = new ClienteRequest();		
		request.setApellido("Soler");
		request.setEmail("risolerh@");
		
		mvc.perform(MockMvcRequestBuilders
	  			.post("/api/clientes")
	  			.content(objectMapper.writeValueAsBytes(request))
	  			.accept(MediaType.APPLICATION_JSON)
	  			.contentType(MediaType.APPLICATION_JSON))
//	      .andDo(print())
	      .andExpect(status().is4xxClientError());

	}
	
	@Test
	void testEditClient() throws JsonProcessingException, Exception {
				
		
		ClienteRequest request = new ClienteRequest();
		request.setNombre("Richard");
		request.setApellido("Soler");
		request.setEmail("risolerh@gmail.com");
		
		LinkedMultiValueMap<String, String> requestParams = new LinkedMultiValueMap<>();		
		requestParams.add("id", "1");
		
		mvc.perform(MockMvcRequestBuilders
	  			.put("/api/clientes/id")
	  			.params(requestParams)
	  			.content(objectMapper.writeValueAsBytes(request))
	  			.accept(MediaType.APPLICATION_JSON)
	  			.contentType(MediaType.APPLICATION_JSON))
//	      .andDo(print())
	      .andExpect(status().isOk());
	}

	
	
	@Test
	void testDeleteClient() throws Exception {
		
		LinkedMultiValueMap<String, String> requestParams = new LinkedMultiValueMap<>();		
		requestParams.add("id", "1");
		
		mvc.perform(MockMvcRequestBuilders
	  			.delete("/api/clientes/id")
	  			.params(requestParams))
//	      .andDo(print())
	      .andExpect(status().isOk());
	}

}
