package mx.afirma.rsolerh.evaluacion.model.entity;

import java.math.BigDecimal;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Data;

@Data
@Entity
@Table(name = "habitacion")
public class HabitacionEntity {

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id; //  Identificador único para cada habitación.
	private String numero; //  Número o código de la habitación.
	private String tipo; // Enum): Tipo de habitación (ej. individual, doble, suite).
	private String descripcion; //  Descripción breve de la habitación.
	private Integer capacidad; //  Número máximo de personas permitidas.
	private BigDecimal precioPorNoche; //  Costo por noche de la habitación.
}
