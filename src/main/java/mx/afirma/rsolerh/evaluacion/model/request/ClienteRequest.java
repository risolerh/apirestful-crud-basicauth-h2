package mx.afirma.rsolerh.evaluacion.model.request;

import java.io.Serializable;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

@Data
public class ClienteRequest implements Serializable {
		
	/**
	 * 
	 */
	private static final long serialVersionUID = -9022249535612697051L;
	
	@NotBlank(message = "Nombre es requerido")
	private String nombre; 
	
	@NotBlank(message = "Nombre es requerido")
	private String apellido;
	
	@NotEmpty(message = "Email es requerido")
	@NotNull(message = "Email es requerido")
	@Email(message = "Formato de email invalido")
	private String email;
		
	private String telefono;
	

}
