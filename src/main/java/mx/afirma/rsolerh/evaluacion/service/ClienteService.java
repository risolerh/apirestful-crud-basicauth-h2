package mx.afirma.rsolerh.evaluacion.service;

import java.util.List;
import java.util.Optional;

import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;
import mx.afirma.rsolerh.evaluacion.model.entity.ClienteEntity;
import mx.afirma.rsolerh.evaluacion.model.request.ClienteRequest;
import mx.afirma.rsolerh.evaluacion.model.response.ClienteResponse;
import mx.afirma.rsolerh.evaluacion.repository.IClienteRepo;


@Service
@Slf4j
public class ClienteService implements IClienteService {

	/**
	 * IClienteRepo
	 */
	@Autowired IClienteRepo clienteRepo;
	
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<ClienteResponse> getAllClients() {
		
		Iterable<ClienteEntity> clientesEntity = clienteRepo.findAll();		
		return new ModelMapper().map(clientesEntity, new TypeToken<List<ClienteEntity>>() {}.getType());
	}


	/**
	 * {@inheritDoc}
	 */
	@Override
	public void editClient(Long id, ClienteRequest request) {
		
		Optional<ClienteEntity> clienteOptional = clienteRepo.findById(id);
		if (!clienteOptional.isPresent()) {
			log.error("No se encontro cliente para editar ...");
			throw new InternalError("No se encontro cliente");
		}
		
		ClienteEntity clienteEntity = new ModelMapper().map(request, ClienteEntity.class);
		clienteEntity.setId(id);
		
		log.debug("Guardando cliente en db...");
		clienteRepo.save(clienteEntity);
	}

	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public Long addClient(ClienteRequest request) {

		ClienteEntity clienteEntity = new ModelMapper().map(request, ClienteEntity.class);				
		log.debug("Guardando cliente en db...");
		clienteRepo.save(clienteEntity);
		log.debug("Id cliente creado: [{}]", clienteEntity.getId());
		return clienteEntity.getId();
	}

	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void deleteClient(Long id) {
		
		Optional<ClienteEntity> clienteOptional = clienteRepo.findById(id);
		if (!clienteOptional.isPresent()) {
			log.error("No se encontro cliente para eliminar ...");
			throw new InternalError("No se encontro cliente");
		}		
		log.debug("eliminando cliente");
		clienteRepo.delete(clienteOptional.get());
	}
	

}
