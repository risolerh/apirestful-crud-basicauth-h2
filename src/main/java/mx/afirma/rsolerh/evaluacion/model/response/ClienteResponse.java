package mx.afirma.rsolerh.evaluacion.model.response;

import java.io.Serializable;
import java.time.LocalDate;

import lombok.Data;

@Data
public class ClienteResponse implements Serializable {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = -5504826864217852536L;
	
	private Long clienteId; 
	private String nombre; 
	private String apellido; 
	private String email; 
	private String telefono; 
	private LocalDate fechaRegistro; 
	
}
