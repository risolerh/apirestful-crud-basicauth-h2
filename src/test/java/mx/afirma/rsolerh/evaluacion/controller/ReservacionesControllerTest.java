package mx.afirma.rsolerh.evaluacion.controller;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.LocalDate;
import java.util.ArrayList;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.util.LinkedMultiValueMap;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import mx.afirma.rsolerh.evaluacion.model.request.ReservacionAddRequest;
import mx.afirma.rsolerh.evaluacion.model.request.ReservacionEditRequest;
import mx.afirma.rsolerh.evaluacion.service.IReservacionService;

import static org.mockito.ArgumentMatchers.any;


@WebMvcTest(ReservacionesController.class)
@AutoConfigureMockMvc(addFilters = false)
class ReservacionesControllerTest {
	
	@Autowired private MockMvc mvc;

	@MockBean private IReservacionService reservacionesServ;
	
	ObjectMapper objectMapper;

	ReservacionesControllerTest(){
		objectMapper = new ObjectMapper();
		objectMapper.registerModule(new JavaTimeModule());
	}
	
	@Test
	void testGetAllClients() throws Exception {
		when(reservacionesServ.getAllReservations())
		.thenReturn(new ArrayList<>());
		
		mvc.perform(MockMvcRequestBuilders
	  			.get("/api/reservaciones/all")
	  			.accept(MediaType.APPLICATION_JSON)
	  			.contentType(MediaType.APPLICATION_JSON))
//	      .andDo(print())
	      .andExpect(status().isOk());
	}

	@Test
	void testGetReservacionesCliente() throws Exception {
		LinkedMultiValueMap<String, String> requestParams = new LinkedMultiValueMap<>();		
		requestParams.add("clientId", "1");
		
		mvc.perform(MockMvcRequestBuilders
	  			.get("/api/reservaciones/cliente/id")
	  			.params(requestParams))
//	      .andDo(print())
	      .andExpect(status().isOk());
	}

	@Test
	void testAddReservationst() throws JsonProcessingException, Exception {
		
		when(reservacionesServ.addReservation(any()))
		.thenReturn(1L);
		
		ReservacionAddRequest request = new ReservacionAddRequest();
		request.setClienteId(1L);
		request.setHabitacionId(2L);
		request.setFechaInicio(LocalDate.now());
		request.setFechaFin(LocalDate.now());

		
		mvc.perform(MockMvcRequestBuilders
	  			.post("/api/reservaciones")
	  			.content(objectMapper.writeValueAsBytes(request))
	  			.accept(MediaType.APPLICATION_JSON)
	  			.contentType(MediaType.APPLICATION_JSON))
//	      .andDo(print())
	      .andExpect(status().isOk());
	}

	@Test
	void testEditClient() throws JsonProcessingException, Exception {
	
		ReservacionEditRequest request = new ReservacionEditRequest();
		request.setClienteId(1L);
		request.setHabitacionId(2L);
		request.setFechaInicio(LocalDate.now());
		request.setFechaFin(LocalDate.now());
		
		
		LinkedMultiValueMap<String, String> requestParams = new LinkedMultiValueMap<>();		
		requestParams.add("id", "1");
		
		mvc.perform(MockMvcRequestBuilders
	  			.patch("/api/reservaciones/id")
	  			.params(requestParams)
	  			.content(objectMapper.writeValueAsBytes(request))
	  			.accept(MediaType.APPLICATION_JSON)
	  			.contentType(MediaType.APPLICATION_JSON))
//	      .andDo(print())
	      .andExpect(status().isOk());
	}

	@Test
	void testDeleteClient() throws Exception {
		LinkedMultiValueMap<String, String> requestParams = new LinkedMultiValueMap<>();		
		requestParams.add("id", "1");
		
		mvc.perform(MockMvcRequestBuilders
	  			.delete("/api/reservaciones/id")
	  			.params(requestParams)
	  			)
//	      .andDo(print())
	      .andExpect(status().isOk());
	}

}
