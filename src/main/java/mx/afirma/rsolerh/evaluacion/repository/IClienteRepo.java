package mx.afirma.rsolerh.evaluacion.repository;

import org.springframework.data.repository.CrudRepository;

import mx.afirma.rsolerh.evaluacion.model.entity.ClienteEntity;

/**
 * ClienteRepo
 * 
 * @author rsole
 *
 */
public interface IClienteRepo extends CrudRepository<ClienteEntity, Long>{

	
}
