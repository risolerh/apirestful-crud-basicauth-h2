package mx.afirma.rsolerh.evaluacion.util;

import java.math.BigDecimal;
import java.time.Duration;
import java.time.LocalDate;

import lombok.extern.slf4j.Slf4j;


@Slf4j
final public class ReservationUtils {
	
	private ReservationUtils(){}
	
	public static BigDecimal calcPriceReservation(LocalDate init, LocalDate end, BigDecimal price) {
		if( init.isAfter(end) ) {
			log.debug("Fechas no coinciden {} vs {}", init.toString(), end.toString());
			throw new InternalError("Fecha de reservacion no coinciden..."); 
		}
		
		long days = Duration.between(init.atStartOfDay(), end.atStartOfDay()).toDays();
		log.debug("Se calcularon [{}] dias * [{}]", ++days, price);
		
		return new BigDecimal(days).multiply(price);
	}
}
