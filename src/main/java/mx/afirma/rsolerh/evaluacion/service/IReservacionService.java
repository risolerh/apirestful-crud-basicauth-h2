package mx.afirma.rsolerh.evaluacion.service;

import java.util.List;

import mx.afirma.rsolerh.evaluacion.model.request.ReservacionAddRequest;
import mx.afirma.rsolerh.evaluacion.model.request.ReservacionEditRequest;
import mx.afirma.rsolerh.evaluacion.model.response.ReservacionResponse;

public interface IReservacionService {
	
	 Long addReservation(ReservacionAddRequest request);
	 void editReservation(Long id, ReservacionEditRequest request);
	 List<ReservacionResponse> getAllReservations();
	 List<ReservacionResponse> getReservacionesCliente(Long clienteId);
	 void deleteReservation(Long id);

}
