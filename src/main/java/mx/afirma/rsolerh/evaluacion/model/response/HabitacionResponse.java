package mx.afirma.rsolerh.evaluacion.model.response;

import java.io.Serializable;
import java.math.BigDecimal;

import lombok.Data;

@Data
public class HabitacionResponse implements Serializable {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = -4106376077459017433L;
	
	
	
	private Long habitacionId;
	private String numero; 
	private String tipo; 
	private String descripcion; 
	private Integer capacidad; 
	private BigDecimal precioPorNoche;
	
}
