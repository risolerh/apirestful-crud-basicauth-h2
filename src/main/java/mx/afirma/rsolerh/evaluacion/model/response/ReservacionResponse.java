package mx.afirma.rsolerh.evaluacion.model.response;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;

import lombok.Data;

@Data
public class ReservacionResponse implements Serializable {
	
	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = -5018414084328491844L;
	
	
	private Long reservacionId;	
	private LocalDate fechaInicio; 
	private LocalDate fechaFin; 
	private BigDecimal total; 
	private String estado;	
	private ClienteResponse cliente; 
	private HabitacionResponse habitacion;

}
