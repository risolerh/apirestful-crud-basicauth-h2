package mx.afirma.rsolerh.evaluacion.service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

import org.modelmapper.Conditions;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;
import mx.afirma.rsolerh.evaluacion.model.entity.ClienteEntity;
import mx.afirma.rsolerh.evaluacion.model.entity.HabitacionEntity;
import mx.afirma.rsolerh.evaluacion.model.entity.ReservaEntity;
import mx.afirma.rsolerh.evaluacion.model.request.ReservacionAddRequest;
import mx.afirma.rsolerh.evaluacion.model.request.ReservacionEditRequest;
import mx.afirma.rsolerh.evaluacion.model.response.ReservacionResponse;
import mx.afirma.rsolerh.evaluacion.repository.IClienteRepo;
import mx.afirma.rsolerh.evaluacion.repository.IHabitacionRepo;
import mx.afirma.rsolerh.evaluacion.repository.IReservacionRepo;
import mx.afirma.rsolerh.evaluacion.util.EstadoReservaEnum;
import mx.afirma.rsolerh.evaluacion.util.ReservationUtils;

@Slf4j
@Service
public class ReservacionService implements IReservacionService {

	@Autowired 
	private IReservacionRepo reservacionRepo;
	
	@Autowired
	private IHabitacionRepo habitacionRepo;
	
	@Autowired
	private IClienteRepo clienteRepo;
	
	
	
	@Override
	public Long addReservation(ReservacionAddRequest request) {
		
		
		log.debug("Habitacion encontrada , generando reservacion ...");
		ModelMapper modelMapper = new ModelMapper();
		modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);

		ReservaEntity reservaEntity = modelMapper.map(request, ReservaEntity.class);
		reservaEntity.setEstado(EstadoReservaEnum.PENDIENTE.getValue()); 
		
		validations(reservaEntity, request.getClienteId(), request.getHabitacionId());
		reservaEntity.setTotal(reservaEntity.getHabitacion().getPrecioPorNoche());
		
		log.debug("Guardando reservacion en db...");
		BigDecimal price = ReservationUtils.calcPriceReservation( 
				reservaEntity.getFechaInicio(), 
				reservaEntity.getFechaFin(),
				reservaEntity.getHabitacion().getPrecioPorNoche() );
		reservaEntity.setTotal(price);

		
		reservacionRepo.save( reservaEntity );
		log.debug("Id reservacion creado: [{}]", reservaEntity.getId());
		
		return reservaEntity.getId();
	}
	
	

	@Override
	public void editReservation(Long id, ReservacionEditRequest request) {
		
		Optional<ReservaEntity> reservationOptional = reservacionRepo.findById(id);
		
		if (!reservationOptional.isPresent() ) {
			log.debug("Reservacion a edita no existe");
			throw new InternalError("Reservacion a edita no existe");
		}
		ReservaEntity reservaEntity = reservationOptional.get(); 	
		
		ModelMapper modelMapper = new ModelMapper();
		modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
		modelMapper.getConfiguration().setPropertyCondition(Conditions.isNotNull());
		modelMapper.map(request, reservaEntity);
		
		validations(reservaEntity, request.getClienteId(), request.getHabitacionId());
		BigDecimal price = ReservationUtils.calcPriceReservation( 
				reservaEntity.getFechaInicio(), 
				reservaEntity.getFechaFin(), 
				reservaEntity.getHabitacion().getPrecioPorNoche() );
		reservaEntity.setTotal(price);
		
		log.debug("Guardando cliente en db...");
		reservacionRepo.save(reservaEntity);
	}

	
	@Override
	public List<ReservacionResponse> getAllReservations() {
		log.debug("GetAllReservations...");
		Iterable<ReservaEntity> reservacions = reservacionRepo.findAll();
		return new ModelMapper().map(reservacions, new TypeToken<List<ReservaEntity>>() {}.getType());
	}

	
	
	@Override
	public void deleteReservation(Long id) {
		Optional<ReservaEntity> clienteOptional = reservacionRepo.findById(id);
		if (!clienteOptional.isPresent()) {
			log.error("No se encontro reservacion para eliminar ...");
			throw new InternalError("No se encontro reservacion para borrar");
		}		
		log.debug("eliminando reservacion");
		reservacionRepo.delete(clienteOptional.get());
	}
	
	
	@Override
	public List<ReservacionResponse> getReservacionesCliente(Long clienteId) {
		log.debug("GetClienteReservaciones...");
		List<ReservaEntity> reservacions = reservacionRepo.getReservacionesCliente(clienteId);
		return new ModelMapper().map(reservacions, new TypeToken<List<ReservaEntity>>() {}.getType());
	}

	
	
	/**
	 * @param reservaEntity
	 * @param clienteId
	 * @param habitacionId
	 */
	private void validations(ReservaEntity reservaEntity, Long clienteId, Long habitacionId) {
		
		log.debug("Validando si existe la habitacion...");
		if (habitacionId != null) {
			Optional<HabitacionEntity> habiOptional = habitacionRepo.findById(habitacionId);		
			if (!habiOptional.isPresent()) {
				log.debug("No se encontro habitacion: {}", habitacionId);
				throw new InternalError("No existe habitacionid: " + habitacionId);
			}			
			reservaEntity.setHabitacion(habiOptional.get());
		}
		
		if ( clienteId != null ) {
			Optional<ClienteEntity> clienteOptional = clienteRepo.findById(clienteId);
			if (!clienteOptional.isPresent()) {
				log.debug("No se encontro idcliente [{}]", clienteId);
				throw new InternalError("No se encontro clienteid: " + clienteId);
			}			
			reservaEntity.setCliente(clienteOptional.get());			
		}
	}




}
