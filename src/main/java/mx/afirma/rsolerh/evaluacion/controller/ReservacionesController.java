package mx.afirma.rsolerh.evaluacion.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import mx.afirma.rsolerh.evaluacion.model.request.ReservacionAddRequest;
import mx.afirma.rsolerh.evaluacion.model.request.ReservacionEditRequest;
import mx.afirma.rsolerh.evaluacion.model.response.ReservacionResponse;
import mx.afirma.rsolerh.evaluacion.service.IReservacionService;

@RestController
@Slf4j
@RequestMapping("/api/reservaciones")
@SecurityRequirement(name = "tech-api")
public class ReservacionesController {
	
	@Autowired
	private IReservacionService reservacionesServ;

	
	@GetMapping("all")
	public List<ReservacionResponse> getAllClients(){
		log.debug("Obteniendo todas las reservaciones");
		return reservacionesServ.getAllReservations();
	}
	
	@GetMapping("cliente/{clienteId}")
	public List<ReservacionResponse> getReservacionesCliente(@Valid @RequestParam Long clientId){
		log.debug("Obteniendo todas las reservaciones");
		return reservacionesServ.getAllReservations();
	}
	
	
	@PostMapping
	public Long addReservationst(@RequestBody ReservacionAddRequest request){
		log.debug("Agregando cliente {}", request.toString());		
		return reservacionesServ.addReservation(request);
	}
	
	
	@PatchMapping("{id}")
	public void editClient(@Valid @RequestBody ReservacionEditRequest request, @RequestParam Long id){
		log.debug("Editando cliente {}", id);		
		reservacionesServ.editReservation(id, request);
	}
	
	
	@DeleteMapping("{id}")
	public void deleteClient(@RequestParam Long id){
		log.debug("Elimienado  clienteid {}", id);		
		reservacionesServ.deleteReservation(id);
	}
	

}
