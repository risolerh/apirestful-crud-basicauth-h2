package mx.afirma.rsolerh.evaluacion.util;

import lombok.Getter;

@Getter
public enum TipoReservacionEnum {

	INDIVIDUAL("individual", 1),
	DOBLE("doble", 2),
	SUITE("suite", 3);
	
	private String value;
	private Integer key;
	
	private TipoReservacionEnum(String value, Integer key) {
		this.value = value;
		this.key = key;
	}
	
	public static TipoReservacionEnum getEnumByKey(Integer key) {
		for ( TipoReservacionEnum v : values() ) {
			if(v.key.equals(key)) return v;
		}
		return null;
	}

}
