package mx.afirma.rsolerh.evaluacion.model.entity;

import java.math.BigDecimal;
import java.time.LocalDate;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import lombok.Data;
import lombok.NoArgsConstructor;



@Data
@NoArgsConstructor
@Entity(name = "reserva")
public class ReservaEntity {

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id; //Identificador único para cada reserva.
	
	private LocalDate fechaInicio; //Fecha de inicio de la reserva.
	private LocalDate fechaFin; //Fecha de fin de la reserva.
	private BigDecimal total; //Costo total de la reserva (calculado).
	private String estado; //Estado de la reserva (ej. pendiente, confirmada, cancelada).
	 
	
	@ManyToOne
	@JoinColumn(name = "clienteId", referencedColumnName="id")
	private ClienteEntity cliente; //Referencia al cliente que realiza la reserva.
	
	@ManyToOne
	@JoinColumn(name = "habitacionId", referencedColumnName="id")
	private HabitacionEntity habitacion;

}
