package mx.afirma.rsolerh.evaluacion.repository;

import org.springframework.data.repository.CrudRepository;

import mx.afirma.rsolerh.evaluacion.model.entity.HabitacionEntity;

public interface IHabitacionRepo extends CrudRepository<HabitacionEntity, Long>{
	
	

}
