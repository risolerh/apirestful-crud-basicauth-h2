package mx.afirma.rsolerh.evaluacion.model.request;

import java.io.Serializable;
import java.time.LocalDate;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

@Data
public class ReservacionAddRequest implements Serializable {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 2299900643698840015L;

	@NotNull
	private Long clienteId;
	
	@NotNull(message = "Se debe ingresar el idHabitacion")
	private Long habitacionId;
	
	
	@NotNull
	@NotEmpty(message = "Fecha incio es requerida")
	private LocalDate fechaInicio;
	
	@NotNull
	@NotEmpty(message = "Fecha fin es requerida")
	private LocalDate fechaFin;
	

}
