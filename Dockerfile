FROM openjdk:17
VOLUME /tmp
EXPOSE 8080
ADD target/evaluacion-0.0.1-SNAPSHOT.jar /usr/share/app.jar
ENTRYPOINT ["/usr/bin/java", "-jar", "/usr/share/app.jar"]