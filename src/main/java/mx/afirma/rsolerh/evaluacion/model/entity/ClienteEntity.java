package mx.afirma.rsolerh.evaluacion.model.entity;

import java.time.LocalDate;

import org.hibernate.annotations.CreationTimestamp;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Data;

@Data
@Entity
@Table(name = "cliente")
public class ClienteEntity {

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id; // Identificador único para cada cliente.
	private String nombre; // Nombre del cliente.
	private String apellido; // Apellido del cliente.
	private String email; // Dirección de correo electrónico del cliente.
	private String telefono; // Número de teléfono del cliente.
	
    @CreationTimestamp
	private LocalDate fechaRegistro; // Fecha en que el cliente se registró en el sistema.
	
}
