package mx.afirma.rsolerh.evaluacion.service;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.fail;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;

import mx.afirma.rsolerh.evaluacion.model.request.ClienteRequest;
import mx.afirma.rsolerh.evaluacion.model.response.ClienteResponse;
import mx.afirma.rsolerh.evaluacion.repository.IClienteRepo;

@SpringBootTest
@AutoConfigureMockMvc(addFilters = false)
class ClienteServiceTest {

	@Autowired private IClienteService clienteServ;
	
	
	@SpyBean private IClienteRepo clienteRepo;
	
	@Test
	void testGetAllClients() {
		List<ClienteResponse> listResponse = clienteServ.getAllClients();		
		assertNotNull(listResponse);
	}

	@Test
	void testEditClient() {
		ClienteRequest request = new ClienteRequest();
		request.setNombre("RichardTesting");
		request.setEmail("newemail@gmail.com");
		request.setTelefono("11111111");
		
		try {
			clienteServ.editClient(100L, request);			
		} catch (Exception e) {
			fail("Exception testEditClient");
		}
		
		
	}

	@Test
	void testAddClientAndDelete() {
		ClienteRequest request = new ClienteRequest();
		request.setNombre("Jose");
		request.setEmail("newemail@gmail.com");
		request.setTelefono("11111111");
		
		Long id = clienteServ.addClient(request);
		
		assertNotNull(id);
		
		try {
			clienteServ.deleteClient(id);
		} catch (Exception e) {
			fail("Not yet implemented");
		}
	}



}
