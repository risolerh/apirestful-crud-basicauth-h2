package mx.afirma.rsolerh.evaluacion.model.request;

import java.io.Serializable;
import java.time.LocalDate;

import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import lombok.Data;

@Data
public class ReservacionEditRequest implements Serializable {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 2299900643698840015L;

	private Long clienteId;
	private Long habitacionId;
	private LocalDate fechaInicio;
	private LocalDate fechaFin;
	
	@Min(value = 1, message = "Valores estadoReservacion 1,2,3")
	@Max(value = 3, message = "Valores estadoReservacion 1,2,3")
	private Integer estado;
	
}
