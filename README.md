# Getting Started

Desarrollo de microservicio utilizando Spring Boot 3 (Java17), gestiona un sistema de reservas para un hotel. La aplicación  continene 2 CRUDs (Crear, Leer, Actualizar, Eliminar) para las reservas y los detalles de los clientes.

Se utilizo una arquitectura "Modelo 3 capas" ( IGU, Logica, Persistencia) para presentar CRUDS basicos.

Se aplicaron los principios SOLID, para reutilizar codigo, facilitar pruebas unitarias y poder realizar extenciones de funcionalidades, asi como realizar correcciones.

Se utilizo JPA que utiliza un patron de diseño Factoria para poder cambiar base de datos facilmente, podria quedarse H2 para desarrollo y junitlocal, luego en produccion colocar otra base de datos mas robusta.


### Requerimientos

* Java JDK 17 ( Spring 3) (validar version con: java -v)
* Spring Boot 3.2
* Maven 
* Para correr Junit test es necesario tener abajo el aplicativo para no ocacionar problema con DB H2
* Contraseña de Spring security ( user/123, admin/admin)

### Comando para correr proyecto en consola

* mvn spring-boot:run

### Comando para correr en docker

* mvn install 
* docker build -t spring-boot-evaluacion:spring-evaluacion .
* docker run -p 8080:8080 spring-boot-evaluacion:spring-evaluacion .
* docker ps 


### Accesos a consola H2

* http://localhost:8080/h2-console2
 
### Acceso a documentacion de API swagger 

 http://localhost:8080/swagger-ui/index.html