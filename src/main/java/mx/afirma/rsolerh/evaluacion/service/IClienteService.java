package mx.afirma.rsolerh.evaluacion.service;

import java.util.List;

import mx.afirma.rsolerh.evaluacion.model.request.ClienteRequest;
import mx.afirma.rsolerh.evaluacion.model.response.ClienteResponse;

/**
 * Generar 
 * 
 * @author rsole
 *
 */
public interface IClienteService {

	List<ClienteResponse> getAllClients();
	void editClient(Long id, ClienteRequest request);
	Long addClient(ClienteRequest request);
	void deleteClient(Long id);
}
