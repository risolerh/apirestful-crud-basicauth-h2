package mx.afirma.rsolerh.evaluacion.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import mx.afirma.rsolerh.evaluacion.model.entity.ReservaEntity;

public interface IReservacionRepo extends CrudRepository<ReservaEntity, Long>{

	@Query("FROM reserva WHERE cliente.id = :id")
	List<ReservaEntity> getReservacionesCliente(@Param("id")Long id);
}
