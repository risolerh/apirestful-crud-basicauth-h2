package mx.afirma.rsolerh.evaluacion.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import mx.afirma.rsolerh.evaluacion.model.request.ClienteRequest;
import mx.afirma.rsolerh.evaluacion.model.response.ClienteResponse;
import mx.afirma.rsolerh.evaluacion.service.IClienteService;

@RestController
@Slf4j
@RequestMapping("/api/clientes")
@SecurityRequirement(name = "tech-api")
public class ClientesController {

	@Autowired
	private IClienteService clienteServ;
	
	@GetMapping("/all")
	public List<ClienteResponse> getAllClients(){
		log.debug("Obteniendo todos los clientes");
		return clienteServ.getAllClients();
	}
	
	
	@PostMapping
	public Long addClient(@Valid @RequestBody ClienteRequest request){
		log.debug("Agregando cliente {}", request.toString());		
		return clienteServ.addClient(request);
	}
	
	
	@PutMapping("{id}")
	public void editClient(@Valid @RequestBody ClienteRequest request, @RequestParam Long id){
		log.debug("Editando cliente {}", id);		
		clienteServ.editClient(id, request);
	}
	
	
	@DeleteMapping("{id}")
	public void deleteClient(@RequestParam Long id){
		log.debug("Elimienado  clienteid {}", id);		
		clienteServ.deleteClient(id);
	}
	
	
	
}
