package mx.afirma.rsolerh.evaluacion.service;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.fail;

import java.time.LocalDate;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;

import mx.afirma.rsolerh.evaluacion.model.request.ReservacionAddRequest;
import mx.afirma.rsolerh.evaluacion.model.request.ReservacionEditRequest;
import mx.afirma.rsolerh.evaluacion.model.response.ReservacionResponse;
import mx.afirma.rsolerh.evaluacion.repository.IClienteRepo;
import mx.afirma.rsolerh.evaluacion.repository.IHabitacionRepo;
import mx.afirma.rsolerh.evaluacion.repository.IReservacionRepo;


@SpringBootTest
class ReservacionServiceTest {

	@Autowired private IReservacionService reservationService;
	
	@SpyBean private IReservacionRepo reservacionRepo;
	
	@SpyBean private IHabitacionRepo habitacionRepo;
	
	@SpyBean private IClienteRepo clienteRepo;
		
	@Test
	void testAddReservationAndDelete() {
		
		ReservacionAddRequest request = new ReservacionAddRequest();
		request.setClienteId(100L);
		request.setFechaInicio(LocalDate.now());
		request.setFechaFin(LocalDate.now());
		request.setHabitacionId(2L);
		
		Long id = reservationService.addReservation(request);
		
		assertNotNull(id);
		
		
		ReservacionEditRequest requestEdit = new  ReservacionEditRequest();
		requestEdit.setHabitacionId(3L);
		
		reservationService.editReservation(id, requestEdit);
		
		try {
			reservationService.deleteReservation(id);
		} catch (Exception e) {
			fail("Not eliminate reservation");
		}
	}



	@Test
	void testGetAllReservations() {
		
		ReservacionAddRequest request = new ReservacionAddRequest();
		request.setClienteId(100L);
		request.setFechaInicio(LocalDate.now());
		request.setFechaFin(LocalDate.now());
		request.setHabitacionId(1L);
		
		Long id = reservationService.addReservation(request);
		
		assertNotNull(id);
		
		List<ReservacionResponse> reservationsList = reservationService.getAllReservations();
		
		assertNotNull(reservationsList);
	}



	@Test
	void testGetReservacionesCliente() {
		ReservacionAddRequest request = new ReservacionAddRequest();
		request.setClienteId(100L);
		request.setFechaInicio(LocalDate.now());
		request.setFechaFin(LocalDate.now());
		request.setHabitacionId(3L);
		
		Long id = reservationService.addReservation(request);
		
		assertNotNull(id);
		
		List<ReservacionResponse> reservationsList = reservationService.getReservacionesCliente(100L);
		
		assertNotNull(reservationsList);
	}

}
